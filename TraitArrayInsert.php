<?php

namespace WPez\WPezTraits\ArrayInsert;

trait TraitArrayInsert {

    protected function arrayInsert( $arr_input = [], $arr_insert = [], $int_col = false ) {

        if ( $int_col === false ){
            $int_col = count($arr_input);
        }

        return array_merge(
            array_slice( (array) $arr_input, 0, $int_col, false ),
            (array) $arr_insert,
            array_slice( (array) $arr_input, $int_col, null, true )
        );

    }
}